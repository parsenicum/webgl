import { Object3D, TorusKnotBufferGeometry, MeshNormalMaterial, Mesh } from 'three'

export default class Torus extends Object3D {
  constructor () {
    super()

    const geometry = new TorusKnotBufferGeometry(1, 0.25, 100, 16)
    const material = new MeshNormalMaterial()
    const mesh = new Mesh(geometry, material)

    this.add(mesh)
  }
}
