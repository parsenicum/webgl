## Usage
After cloning install all node dependencies
```bash
npm i
```

Then launch the main task to open budo livereload server  
```bash
npm start
```

You are good to go !

If you need a minified build just run
```bash
npm run build
```
Then put the `/index.html` and `/index.js` (+ any assets that you might be using) on your server.
